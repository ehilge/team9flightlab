/* this .c file contains function needed to run the state machine*/

#define EXTERN
#define FOLLOW_H 2
#include "command.h"

/*set the servos to a particular value entered into pos1*/
int setServo(double *pos1, const double *servoZeroDeg, double *servoResolution, double *pw, double *last_pos){
  int servo = 1;
  int stat = 1;
  double theta[3];

  //ensure command does not volate limits of the workspace
    if ( abs(pos1[0])>XLIM || abs(pos1[1])>YLIM || pos1[2] > -2)
    {
      printf("Invalid command in set Servo\n");
      return -1;
    }

  //be doubly sure that the desired location is within the workspace
    stat = delta_calcInverse(pos1[0], pos1[1], pos1[2], (theta+2), (theta+0), (theta+1));
    if (stat){
      printf("unreachable point in setServo\n");
      return -1;
    }

    // calculate necessary PWM signal to send to each servo
    pw[0] = servoZeroDeg[0] + servoResolution[0]*theta[0];
    pw[1] = servoZeroDeg[1] + servoResolution[1]*theta[1];
    pw[2] = servoZeroDeg[2] + servoResolution[2]*theta[2];

    // Loop through each servo and command the PWM
    for(servo = 1; servo<=3; servo++){
      bbb_setPeriodPWM(servo, 20000000);  // Period is in nanoseconds
      bbb_setDutyPWM(servo, pw[servo-1]);  // Duty cycle percent (0-100)
      bbb_setRunStatePWM(servo, 1);
      last_pos[servo-1] = pos1[servo-1];
    }

return 0;
}


//commands the actuator to go to hover above the target
//if threshhold conditions are met, then flag the state machine to attempt a grab
void follow(state_t *state, double *pos, double *theta, const double *servoZeroDeg, double *servoResolution, double *pw,
            double *limit, int *goforit, double *last_pos){

  int statA, statB, statC, statD;
  int m;
  double accel[3];
  double gyro[3];
  double angles[3]={0};

  //Calculate how far away the target is from the quadcopter
  pos[0] = gsl_vector_get(state->targetPos,0);
  pos[1] = gsl_vector_get(state->targetPos,1);
  pos[2] = gsl_vector_get(state->targetPos,2)+9.25;

  //Position the servo so it is some distance above the target
  statA = delta_calcInverse(pos[0], pos[1], pos[2], (theta+2), (theta+0), (theta+1));
  if(statA==-1){ //then point is currently unreachable, get somewhat close
    //purpose of this statement is to set the desired point to be at the limit
    // of the reachable space if it is currently unreachable
    for(m=0;m<2;m++){
      if (pos[m] > limit[m]){
        pos[m] = limit[m];
      }
    }

    pos[2] = -4; //set z to some neutral height

    //run inverse kinematics again to get a new set of (valid) angles to command the servos
    statB = delta_calcInverse(pos[0], pos[1], pos[2], (theta+2), (theta+0), (theta+1));

    //inverse kinematics should produce a valid result here 
    if(statB){
      printf("You've got a problem i nunreachale\n");
    }
  }

  else if (statA==0) {
    //keep the x and y positions the same, but try to hover a certain height above the target
    pos[2] = pos[2] + 1;
    //run inverse kinematics again to get a new set of (valid) angles to command the servos
    statC = delta_calcInverse(pos[0], pos[1], pos[2], (theta+2), (theta+0), (theta+1));
    if(statC){
      printf("You've got a problem in reachable\n");
    }
  } //by this point, angles that the servos need to be commanded at are all defined in theta

  //command the servos to go to their position
  statD = setServo(pos, servoZeroDeg, servoResolution, pw, last_pos);
  if(statD){
    printf("bad position assignemnt at the end of command\n");
    return;
  }
  printf("arm sent to position\n");

  // give time for the servos to get there
  usleep(100000);

  // retreive IMU data and perform calculations
  accel[0] = *(state->accel[0]->filt);
  accel[1] = *(state->accel[1]->filt);
  accel[2] = *(state->accel[2]->filt);

  gyro[0] = *(state->gyro[0]->filt)-0.267;
  gyro[1] = *(state->gyro[1]->filt)-0.0335;
  gyro[2] = *(state->gyro[2]->filt)+4.241;

  calcAttitude(accel[0], accel[1], accel[2], angles, angles+1, angles+2);
  angles[0] = angles[0] * 57.2957795131;
  angles[1] = angles[1] * 57.2957795131;
  angles[2] = angles[2] * 57.2957795131;

  //if all conditions are met then set goforit flag to one, this indicates that
  //the robot should try to geab the ball in flightCode.c
  if( (abs(angles[2])<15 && abs(gyro[1])<5 && abs(gyro[2])<5 && abs(gyro[3])<5)){
    *goforit=1;
  }
}




//Function to do a simple trajectory plan
void trajectory (double *pos_0, double *pos_f, int num_pts,
                 const double *servoZeroDeg, double *servoResolution, double *pw, double *last_pos){

double t=0;
double p[3];
int stat;


 // loop though desired number of points and send servo commands
 for(t=0; t<=1; t = t+1.0/(double)num_pts){

  printf("\n t: %lf", t);
  p[0] = pos_0[0] + 3*(pos_f[0]-pos_0[0])*t*t - 2*(pos_f[0]-pos_0[0])*t*t*t;
  p[1] = pos_0[1] + 3*(pos_f[1]-pos_0[1])*t*t - 2*(pos_f[1]-pos_0[1])*t*t*t;
  p[2] = pos_0[2] + 3*(pos_f[2]-pos_0[2])*t*t - 2*(pos_f[2]-pos_0[2])*t*t*t;

  printf("\n Three positions: %lf %lf %lf\n", p[0], p[1], p[2]);
  stat = setServo(p, servoZeroDeg, servoResolution, pw, last_pos);
  if (stat==-1) continue;

  // give a discrete amount of tme before each servo command is sent
  usleep(10000/num_pts);
 }

}
