/* Example of receiving LCM messages */

#define EXTERN

#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>

#include "lcmtypes/imu_t.h"
#include "lcmtypes/pixy_frame_t.h"

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
    printf("Received message on channel %s, timestamp %" PRId64 "\n",
           channel, msg->utime);
    printf("Number of objects: %d\n", msg->nobjects);
    for (int i = 0; i < msg->nobjects; i += 1) {
        pixy_t *obj = &msg->objects[i];

        printf("%d: ", i);
        if (obj->type == PIXY_T_TYPE_COLOR_CODE) {
            printf("Color code %d (octal %o) at (%d, %d) size (%d, %d)"
                   " angle %d\n", obj->signature, obj->signature,
                   obj->x, obj->y, obj->width, obj->height, obj->angle);
        } else {
            printf("Signature %d at (%d, %d) size (%d, %d)\n",
                   obj->signature, obj->x, obj->y, obj->width, obj->height);
        }
    }
}

void imu_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                  const imu_t *msg, void *userdata)
{
    printf("Received message on channel %s\n", channel);
    printf("gyro (deg/sec) = (%.3lf, %.3lf, %.3lf),\n ", msg->gyro[0],msg->gyro[1], msg->gyro[2]);
    printf("accel (g) = (%.3lf, %.3lf, %.3lf), \n", msg->accel[0], msg->accel[1], msg->accel[2]);
}

int main()
{
    lcm_t *lcm = lcm_create(NULL);

    pixy_frame_t_subscribe(lcm, "PIXY", pixy_handler, NULL);
    imu_t_subscribe(lcm, "imu", imu_handler, NULL);

    // Enter read loop
    while (1) {
        lcm_handle(lcm);
    }

    lcm_destroy(lcm);
}
