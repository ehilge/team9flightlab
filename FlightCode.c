/*
Evan Hilgemann
Prasanna Kumar
Qinan Cao

This script manages the state machine for the mmanipulator. There are 
two primary threads. One waits for a user input and the other executes
the command requests. A handful of other threads oversee tasks
relating to LCM and data sharing


Options:
1 - grab: Seek for the target, position the end effector so it is above the target in x,y
    when close enough and some conditions are met actuate and return to being above the ball
2 - open: Opens the actuator, used primarily if the grab rountine is unsuccessful and the
    mechanism needs to be reset
3 - drop: Essentially the same as grab, but after conditions are met, the end effector will open
    to drop the ball into the cup
4 - stow: Returns the end effector to its home position regardless of what else is happening
0 - Wait for a command
101 - exits the thread to the program can exit gracefully

*/

#define EXTERN  // Needed for global data declarations in bbb.h

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "command.h"

// Include necessary gsl libraris
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>


//Define variables that are used to calculate the pwm signals
const double servoZeroDeg[3] = {7.5, 7.5, 7.5};
const double servoNintyDeg[3] = {12, 12.1, 12.1};
double servoResolution[3];

#define CLEAR_HEIGHT 3

//Include custom .h files
#include "bbblib/bbb.h"
#include "bbblib/kinematics.h"
#include "pixy_example_threaded.h"

//variables needed for the state machine
int command=0;  //integer value that corresponds to a particular execution
pthread_mutex_t flight_mutex;
int msg_count = 0;

/* beginning of the execute thread*/

void *execute(void *data){

state_t *state = data;

//variables needed to command the servos
double num_pts = 100;     //number of points along a trajectory
double pos[3];            //refer to position of end effector
double theta[3];          // commanded servo angles
int stat;                 // status
double pw[3] = {0, 0, 0}; // pulsewidth

int goforit = 0;          // flag
double limit[2] = {XLIM, YLIM};  //set x and y limits


  //While statement has the effect of exiting the thread if '101' is inputted by the user
  while(command!=101){

////////////////////////////////////////////////////////////////////////

    // execution of the grab
    if(command==1){

     // Enter follow function as long as command remains 1
     follow(state, pos,theta,servoZeroDeg, servoResolution, pw, limit, &goforit, &(state->pos_last));

     //if conditions are met in 'follow', then grab the ball
     if(goforit==1){
        //use trajectory plan to drop the end effector by two inches
        pos[2] = pos[2]+2;
        trajectory(&(state->pos_last), pos, num_pts, servoZeroDeg, servoResolution, pw, &(state->pos_last));

        // reset variables so nothing breaks
        goforit=0;

        //stow arm after you're done
        pthread_mutex_lock(&flight_mutex);
        command = 4;
        pthread_mutex_unlock(&flight_mutex);
        usleep(100000);
      }
    }


////////////////////////////////////////////////////////////////////////////////

    //Open the end effector
    else if (command==2){
      // set the Continuous servo to a 25% duty cycle for 0.86 sec to 
      // complete one full rotation

      bbb_setPeriodPWM(0, 20000000);  // Period is in nanoseconds
      bbb_setDutyPWM(0, 25);  // set duty cycle to 99 to make cam spin
      bbb_setRunStatePWM(0, 1);
      usleep(860000); //wait long enough for it to open

      bbb_setDutyPWM(0, 100);  // reset to 100 to stop the spinning
      bbb_setRunStatePWM(0, pwm_run);

      sleep(1);

      //stow when complete
      pthread_mutex_lock(&flight_mutex);
      command = 4;
      pthread_mutex_unlock(&flight_mutex);
      sleep(0.5);
    }

////////////////////////////////////////////////////////////////////////////////

    //position end effector above target and drop the ball
    else if (command==3){
      // enter the follow function
      follow(state, pos,theta,servoZeroDeg, servoResolution, pw, limit, &goforit, &(state->pos_last));

      // when conditions permit, reset goforit flag and open the end effector
      if(goforit==1){
          goforit = 0;
          command = 2;
        }
    }

////////////////////////////////////////////////////////////////////////////////
    
    //Stow command
    else if (command==4){

      // set desired end effector position
      pos[0] = 0;
      pos[1] = 0;
      pos[2] = STO_H;

      stat = setServo(pos, servoZeroDeg, servoResolution, pw, &(state->pos_last));
      if (stat==-1){
         printf("FAIL\n");
         continue;
      }

      printf("arm stowed\n");

      //reset the command to neutral state so it doesn't try to do anything stupid
      pthread_mutex_lock(&flight_mutex);
      command=0;
      pthread_mutex_unlock(&flight_mutex);

    }


////////////////////////////////////////////////////////////////////////////////

   //default state, wait for input
    else {
    }

  }//end infinite while loop, allows program to exit gracefully

return NULL;

}//end thread



//input thread simply waits for an input from the user 
void *input(void *arg)
{
  while(command!=101){
        scanf("%d", &command);
  }
return NULL;
}

/////////////////////////////////////////////////////////////////
/* the following threads deal with publishing and receiving LCM*/
/////////////////////////////////////////////////////////////////


void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
    //printf("Received message on channel %s, timestamp %" PRId64 "\n",
      //     channel, msg->utime);

        state_t *state = userdata;

        int i;
    // Do some processing of the pixy data here
    // Caution: don't save the msg pointer; the data won't be valid after
    //  this handler function exists
    //printf("Number of objects: %d\n", msg->nobjects);

	//printf("state objects: %d\n", state->nobject);
    state->nobject = msg->nobjects;
/*	for (i = 0; i < TARGET_NUM; i += 1) {
		free(state->pixy_x[i]->filt);
		free(state->pixy_y[i]->filt);
		free(state->pixy_width[i]->filt);
		free(state->pixy_height[i]->filt);
		//state->pixy_angle[i]->filt = NULL;
	}
*/
    for (i = 0; i < msg->nobjects; i += 1) {
        pixy_t *obj = &msg->objects[i];

        //printf("number of object = %d: ", i+1);

		// Determine which target
        int target;
        if (obj->signature == TARGET_0) target = 0;
        else if(obj->signature == TARGET_1) target = 1;
        else if(obj->signature == TARGET_2) target = 2;
        else if(obj->signature == TARGET_3) target = 3;
        else continue;

        if (obj->type == PIXY_T_TYPE_COLOR_CODE) {
/*            printf("Color code %d (octal %o) at (%d, %d) size (%d, %d)"
                   " angle %d\n", obj->signature, obj->signature,
                   obj->x, obj->y, obj->width, obj->height, obj->angle);
			DataFilter(obj->x, state->pixy_x[target]->raw, state->pixy_x[target]->med, state->pixy_x[target]->filt); 
			DataFilter(obj->y, state->pixy_y[target]->raw, state->pixy_y[target]->med, state->pixy_y[target]->filt); 
			DataFilter(obj->width, state->pixy_width[target]->raw, state->pixy_width[target]->med,state->pixy_width[target]->filt); 
			DataFilter(obj->height, state->pixy_height[target]->raw, state->pixy_height[target]->med, state->pixy_height[target]->filt); 
*/
			//DataFilter(obj->angle, state->pixy_angle[target]->raw, state->pixy_angle[target]->med, state->pixy_angle[target]->filt); 
        } 
		else {
     //       printf("Signature %d at (%d, %d) size (%d, %d)\n",
       //            obj->signature, obj->x, obj->y, obj->width, obj->height);
        }
    }

    if((int)calculateZ(state)==1)
        targetLocationQuadrotor(state, state->targetPos);

    // Lock mutex before modifying shared data
    pthread_mutex_lock(&flight_mutex);

    // Dumb example of modifying shared data in a global variable
    msg_count += 1;

    pthread_mutex_unlock(&flight_mutex);
}

void imu_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                  const imu_t *msg, void *userdata)
{
	state_t *state = userdata;
    //printf("Received message on channel %s\n", channel);
    //printf("gyro (deg/sec) = (%.3lf, %.3lf, %.3lf),\n ", msg->gyro[0],msg->gyro[1], msg->gyro[2]);
    //printf("accel (g) = (%.3lf, %.3lf, %.3lf), \n", msg->accel[0], msg->accel[1], msg->accel[2]);
	DataFilter(msg->gyro[0], state->gyro[0]->raw, state->gyro[0]->med, state->gyro[0]->filt);
	DataFilter(msg->gyro[1], state->gyro[1]->raw, state->gyro[1]->med, state->gyro[1]->filt);
	DataFilter(msg->gyro[2], state->gyro[2]->raw, state->gyro[2]->med, state->gyro[2]->filt);
	DataFilter(msg->accel[0], state->accel[0]->raw, state->accel[0]->med, state->accel[0]->filt);
	DataFilter(msg->accel[1], state->accel[1]->raw, state->accel[1]->med, state->accel[1]->filt);
	DataFilter(msg->accel[2], state->accel[2]->raw, state->accel[2]->med, state->accel[2]->filt);
}

void* lcm_receive_loop(void *data)
{
	state_t *state = data;      

    pixy_frame_t_subscribe(state->lcm, "PIXY", pixy_handler, state);
	imu_t_subscribe(state->lcm, "imu", imu_handler, state);

    while (1) {
        lcm_handle(state->lcm);
    }

    lcm_destroy(state->lcm);
}

void* processing_loop(void *data)
{
	state_t *state = data;
	int hz = 1;

    while (1) {

        // Lock the mutex before reading shared data
        pthread_mutex_lock(&flight_mutex);

 //       printf("msg_count: %d\n", msg_count);

        // We're done -- unlock the mutex
        pthread_mutex_unlock(&flight_mutex);

        usleep(1000000/hz);
    }
}

state_t *
state_create (void)
{
    state_t *state = calloc(1,sizeof(*state));
    // Data
    for (int k = 0; k < TARGET_NUM; k++) {
		state->pixy_x[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_x[k]->raw[NUM_MED] = 0;
		state->pixy_x[k]->med[NUM_MED] = 0;
		state->pixy_x[k]->filt = calloc(1,sizeof(double));
		state->pixy_y[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_y[k]->raw[NUM_MED] = 0;
		state->pixy_y[k]->med[NUM_MED] = 0;
		state->pixy_y[k]->filt = calloc(1,sizeof(double));
		state->pixy_width[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_width[k]->raw[NUM_MED] = 0;
		state->pixy_width[k]->med[NUM_MED] = 0;
		state->pixy_width[k]->filt = calloc(1,sizeof(double));
		state->pixy_height[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_height[k]->raw[NUM_MED] = 0;
		state->pixy_height[k]->med[NUM_MED] = 0;
		state->pixy_height[k]->filt = calloc(1,sizeof(double));
		state->pixy_angle[k] = calloc(1,sizeof(pixy_data_t));
		state->pixy_angle[k]->raw[NUM_MED] = 0;
		state->pixy_angle[k]->med[NUM_MED] = 0;
		state->pixy_angle[k]->filt = calloc(1,sizeof(double));
    }

    for (int k = 0; k < 3; k++) {
		state->gyro[k] = calloc(1,sizeof(imu_data_t));
		state->gyro[k]->raw[NUM_MED] = 0;
		state->gyro[k]->med[NUM_MED] = 0;
		state->gyro[k]->filt = calloc(1,sizeof(double));
		state->accel[k] = calloc(1,sizeof(imu_data_t));
		state->accel[k]->raw[NUM_MED] = 0;
		state->accel[k]->med[NUM_MED] = 0;
		state->accel[k]->filt = calloc(1,sizeof(double));
	}
	state->nobject = 0;
	state->Z = 0; 
	state->Zprev = 100;
	state->indices = calloc(4, sizeof(int));
	state->targetPos = gsl_vector_calloc(4);

    // lcm
    state->lcm = lcm_create(NULL);

    //last commanded sero position
    state->pos_last=0;

    return state;
}




/////////////////////////////////////////////////////////////////
/* main function initializes everything an starts the thread*/
/////////////////////////////////////////////////////////////////


int main(void){


double pos[3];
int stat;
double pw[3];
  int servo=1;

  //calculating servo resolutions
  for(servo=1; servo<=3; servo++){
	servoResolution[servo-1] = (servoNintyDeg[servo-1] - servoZeroDeg[servo-1])/90;
  }

  //initialize the beaglebone
  if (bbb_init()) {
    printf("Error initializing BBB.\n");
    return -1;
  }

  //initialize each of the servos
  for(servo=0;servo<4; servo++){
    if (bbb_initPWM(servo)) {
      printf("Error initializing BBB PWM pin %d. Are you running as root?\n",servo);
    }
  }

  //Stow the end effector
  pos[0] = 0;
  pos[1] = 0;
  pos[2] = STO_H; 

  double dummy[3];

  stat = setServo(pos, servoZeroDeg, servoResolution, pw, dummy );
  if (stat==-1){
     printf("FAIL\n");
  }

  // set continuous servo to not spin
  bbb_setPeriodPWM(0, 20000000);  // Period is in nanoseconds
  bbb_setDutyPWM(0,100);  // Duty cycle percent (0-100)
  bbb_setRunStatePWM(0, 1);

  state_t *state = state_create();    

  //initialize mutex
  pthread_mutex_init(&flight_mutex, NULL);

  //create threads
  pthread_t ex;  // this is our thread identifier
  pthread_create(&ex,NULL,execute, state);

  pthread_t in;	// this is our thread identifier
  pthread_create(&in,NULL,input,NULL);

  pthread_t lcm_receive_thread;
  pthread_t processing_thread;
  pthread_create(&lcm_receive_thread, NULL, lcm_receive_loop, state);
  pthread_create(&processing_thread, NULL, processing_loop, state);

  // wait for threads to retun NULL before exiting
  pthread_join(ex, NULL );
  pthread_join(in, NULL );
  pthread_join(lcm_receive_thread, NULL);

  pthread_mutex_destroy(&flight_mutex);


  //clean up servos set to stow position

  pos[0] = 0;
  pos[1] = 0;
  pos[2] = STO_H; 

  stat = setServo(pos, servoZeroDeg, servoResolution, pw,pos);
  if (stat==-1){
     printf("FAIL\n");
  }

  sleep(1);

  for (servo=0; servo<4; servo++){
    bbb_setRunStatePWM(servo, pwm_stop);
  }

}
