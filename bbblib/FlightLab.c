 /*
 *Evan Hilgemann
  * Prasan Kumar
  * Qinan
  */

#define EXTERN  // Needed for global data declarations in bbb.h
#include "bbb.h"

const double servoZeroDeg[3] = {7.5, 7.5, 7.5};
const double servoNintyDeg[3] = {12, 12.1, 12.1};
double servoResolution[3];

int main() 
{

  //calculating resolutions
  int servo = 1;
  for(servo; servo<=3; servo++)
	servoResolution[servo-1] = (servoNintyDeg[servo-1] - servoZeroDeg[servo-1])/90;

  double pos[3];
  double theta[3];

  int stat;
  double pw[3] = {0, 0, 0};
  int signal;
 
  while(1)
  {
	printf("Enter desired X, Y & Z positions:\t");
	scanf("%lf %lf %lf", &pos[0], &pos[1], &pos[2]);  
	stat = delta_calcInverse(pos[0], pos[1], pos[2], (theta+2), (theta+0), (theta+1));
	if(stat){
		printf("Unreachable point!!\n"); 
		continue; 
	}
		
	printf("Angles: [%lf, %lf, %lf]\n", theta[0], theta[1], theta[2]);
	printf("Commanding the links to the angles...\n");
	pw[0] = servoZeroDeg[0] + servoResolution[0]*theta[0];
	pw[1] = servoZeroDeg[1] + servoResolution[1]*theta[1];
	pw[2] = servoZeroDeg[2] + servoResolution[2]*theta[2];
	printf("Pulses: [%lf, %lf, %lf], All good ?  ", pw[0], pw[1], pw[2]);
	scanf("%d", &signal);
	if(signal)	
	{	
	if (bbb_init()) 
	{
    		printf("Error initializing BBB.\n");
	}

	for(servo = 1; servo<=3; servo++)
	{
		 if (bbb_initPWM(servo)) 
		{
    		printf("Error initializing BBB PWM pin %d. Are you running as root?\n", servo);
		//    return -1;
  		}

		bbb_setPeriodPWM(servo, 20000000);  // Period is in nanoseconds
		bbb_setDutyPWM(servo, pw[servo-1]);  // Duty cycle percent (0-100)
		bbb_setRunStatePWM(servo, pwm_run);
  
		printf("Running PWM at %.1f percent for 10 seconds...\n",
        	bbb_getDutyPWM(servo));
	} 
	}
	
}
	/*printf("Enter PWM channel #.\n");
  	//scanf("%d %lf", &PWM_Choice, &perc);

  	// Initialize BBB and one PWM channel

  if (bbb_init()) {
    printf("Error initializing BBB.\n");
   // return -1;
  }

  if (bbb_initPWM(PWM_Choice)) {
    printf("Error initializing BBB PWM pin %d. Are you running as root?\n", PWM_Choice);
//    return -1;
  }

  // Setup PWM (ask user for duty cycle %)
//  printf("Enter PWM duty cycle in percent (0-100):\n");
//  scanf("%d", &perc);

  // The PWM hardware is set up so that channels 0 and 1 share the same period
  // and run state, and channels 2 and 3 are similarly paired
  bbb_setPeriodPWM(PWM_Choice, 20000000);  // Period is in nanoseconds
  bbb_setDutyPWM(PWM_Choice, perc);  // Duty cycle percent (0-100)
  bbb_setRunStatePWM(PWM_Choice, pwm_run);

  printf("Running PWM at %.1f percent for 10 seconds...\n",
	 bbb_getDutyPWM(PWM_Choice));
}
//  sleep(10);  // Run for 10 seconds

  // Stop PWM & exit

//  bbb_setDutyPWM(PWM_CHOICE, 0);

*/
  return 0;
}
