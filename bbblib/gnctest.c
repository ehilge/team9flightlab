#include <math.h>
#include <stdio.h>
#include "gnc.h"


#define EXTERN
//#define NUM_MED 5
//#define NUM_AVG 3

int main(){

double meas[11] = {1.1,1.3,1.15,2.3,74,2.1,1.8,2,1.6,9.8,10};
double current_meas;
int i=0,k=0;
double raw[NUM_MED]={0};
double med[NUM_MED]={0};
double *filt = (double*)calloc(1, sizeof(double));

printf("measured values: \n");

for (i;i<11;i++){
  printf("%lf  ", meas[i]);
}

printf("\nFiltered values:\n");

for (i=0; i<11; i++){
  current_meas = meas[i];

  DataFilter(current_meas, raw, med, filt); 
  printf("\n \n");

  //for (k=0;k<NUM_MED; k++){
  //  printf("%lf ",raw[k]);
  //}
  printf(" %lf", *filt);

}
//num = median(meas, 8);

printf("\n\n");

free(filt);
return 0;
}
