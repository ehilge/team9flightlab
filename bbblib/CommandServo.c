 /*
  *  Beaglebone Black Hardware Interface Function Library
  *  For Use in Robotics 550, University of Michigan
  *
  *  References:  
  *      http://beagleboard.org/
  *      BlackLib: http://free.com/projects/blacklib
  *
  *  bbb_test_PWM.c:  Tests PWM for one servo channel
  *
  */

#define EXTERN  // Needed for global data declarations in bbb.h
#include "bbb.h"

//#define PWM_CHOICE PWM_SV2  // set up choice of channel for each test

int main()
{
  int i;
  double perc;
  int PWM_Choice;

  while(1){
  printf("Enter PWM channel # and PWM percentage.\n");
  scanf("%d %lf", &PWM_Choice, &perc);

  // Initialize BBB and one PWM channel

  if (bbb_init()) {
    printf("Error initializing BBB.\n");
   // return -1;
  }

  if (bbb_initPWM(PWM_Choice)) {
    printf("Error initializing BBB PWM pin %d. Are you running as root?\n", PWM_Choice);
//    return -1;
  }

  // Setup PWM (ask user for duty cycle %)
//  printf("Enter PWM duty cycle in percent (0-100):\n");
//  scanf("%d", &perc);

  // The PWM hardware is set up so that channels 0 and 1 share the same period
  // and run state, and channels 2 and 3 are similarly paired
  bbb_setPeriodPWM(PWM_Choice, 20000000);  // Period is in nanoseconds
  bbb_setDutyPWM(PWM_Choice, perc);  // Duty cycle percent (0-100)
  bbb_setRunStatePWM(PWM_Choice, pwm_run);

  printf("Running PWM at %.1f percent for 10 seconds...\n",
	 bbb_getDutyPWM(PWM_Choice));
}
//  sleep(10);  // Run for 10 seconds

  // Stop PWM & exit

//  bbb_setDutyPWM(PWM_CHOICE, 0);

  return 0;
}
