/*
 *Evan Hilgemann
  * Prasan Kumar
  * Qinan
  */

#define EXTERN  // Needed for global data declarations in bbb.h
#include "bbb.h"

const double servoZeroDeg[3] = {4.9, 5.4, 5.3};
const double servoNintyDeg[3] = {10.4, 10.8, 10.7};
double servoResolution[3];

int main() 
{

  int servo = 1;
  for(servo; servo<=3; servo++)
        servoResolution[servo-1] = (servoNintyDeg[servo-1] - servoZeroDeg[servo-1])/90;
  
  double pos[3];
  double theta[3];

  int stat;
  double pw[3] = {0, 0, 0};
  int signal;
 
  while(1)
  {
	printf("Enter desired angles:\t");
	scanf("%lf %lf %lf", &theta[0], &theta[1], &theta[2]);  
	stat = delta_calcForward(theta[0], theta[1], theta[2], pos, (pos+1), (pos+2));
	printf("Point: %lf, %lf, %lf\n", pos[0], pos[1], pos[2]);
	
	printf("Angles: [%lf, %lf, %lf]\n", theta[0], theta[1], theta[2]);
	printf("Commanding the links to the angles...\n");
	pw[0] = servoZeroDeg[0] + servoResolution[0]*theta[0];
	pw[1] = servoZeroDeg[1] + servoResolution[1]*theta[1];
	pw[2] = servoZeroDeg[2] + servoResolution[0]*theta[2];
	printf("Pulses: [%lf, %lf, %lf], All good ?  ", pw[0], pw[1], pw[2]);
	scanf("%d", &signal);
	if(signal)	
	{	
		if (bbb_init()) 
		{
    			printf("Error initializing BBB.\n");
		}

		for(servo = 1; servo<=3; servo++)
		{
			if (bbb_initPWM(servo)) 
    				printf("Error initializing BBB PWM pin %d. Are you running as root?\n", servo);

			bbb_setPeriodPWM(servo, 20000000);  // Period is in nanoseconds
			bbb_setDutyPWM(servo, pw[servo-1]);  // Duty cycle percent (0-100)
			bbb_setRunStatePWM(servo, pwm_run);
  
			printf("Running PWM at %.1f percent for 10 seconds...\n",
        		bbb_getDutyPWM(servo));
		} 
		
	}
}

	return 0;
}
