#ifndef __KINEMATICS__
#define __KINEMATICS__

#include<stdio.h>
#include<math.h>

#define NUM_MED 2
#define NUM_AVG 2

//Function to calculate angles from accelerometer data
// theta = angle between the horizon and x axis
// psi = angle between horizontal and y axis
// phi = angle between the vertical (gravity vector) and z axis)
void calcAttitude(double x, double y, double z, double *theta, double *psi, double *phi); 

// Filering functions
double median(double *arr, int n);
double avg(double *arr, int n);
void DataFilter(double meas, double raw_data[], double med_data[], double *filt);


#endif
