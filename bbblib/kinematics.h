#ifndef __KINEMATICS_H__
#define __KINEMATICS_H__

#include<stdio.h>
#include<math.h>



// Robot geometry (assumes identical links) 
// --> watch these globals - may want to add identifiers (e.g., "delta_f", "delta_e"...)
static const double f = 5.2;     // base reference triangle side length
static const double e = 5.6;     // end effector reference triangle side length
static const double rf = 4.0;    // Top link length
static const double re = 6.0;    // Bottom link length
 
// trigonometric constants
static const double pi = 3.141592653;    // PI
static const double sqrt3 = 1.732051;    // sqrt(3)
static const double sin120 = 0.866025;   // sqrt(3)/2
static const double cos120 = -0.5;        
static const double tan60 = 1.732051;    // sqrt(3)
static const double sin30 = 0.5;
static const double tan30 = 0.57735;     // 1/sqrt(3)

// servo resolutions
//const double res_servo1 = 0.055;
//const double res_servo2 = 0.05889;
//const double res_servo3 = 0.05778;

int delta_calcForward(double theta1, double theta2, double theta3, double *x0, double *y0, double *z0); 

int delta_calcInverse(double x0, double y0, double z0, double *theta1, double *theta2, double *theta3); 

int delta_calcAngleYZ(double x0, double y0, double z0, double *theta);

#endif
