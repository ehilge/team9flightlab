// This .c file defines the calculations needed to determine the current angle of the
// platform from accelerometer measurements as well as median/mean filter.

#include<math.h>
#include<stdio.h>
#include"gnc.h"

#define EXTERN

//Function takes in the x, y, and z measurements from the accelerometer and estimates
//orientation angles theta, psi, and phi.
// Theta = angle between x axis and horizon
// psi = angle between y axis and horizon
// phi = angle between z axis and gravity vector
void calcAttitude (double x, double y, double z, double *theta, double *psi, double *phi){
 // printf("x: %lf y:%lf  z:%lf\n\n", x,y,z);
  
  *theta = atan2(x , sqrt(y*y + z*z));
  *psi   = atan2(y , sqrt(x*x + z*z));
  *phi   = atan2( sqrt(x*x + y*y) , z);
  
 // printf("theta: %lf psi:%lf  phi:%lf\n\n", theta,psi,phi);

  return;
}



// Function to find the median of an array of size n
double median(double *arr, int n){

  int i=0 ,j=0;  // counters
  double a = 0; // placeholder variable
  double med = 0;
  double arr2[n];

  for(i=0;i<n;i++){
    arr2[i] = arr[i];
  }

  for (i=0; i<n; i++){
    for (j = i+1; j<n; j++){
      if ( arr2[i] > arr2[j]){
        a = arr2[i];
        arr2[i] = arr2[j];
        arr2[j] = a;
      }//endif
    }//end inner for
  }//end outer for

  i=0;

  if (n % 2){ //n is an odd number, the median is n/2 + 1 when taking into account itneger rounding
    med = arr2[(n/2+1)-1];
  }
  else {//n is even, median is the average of n/2 and (n/2+1)
  //  printf("\n array index is %d\n", n/2-1);

    med = (arr2[n/2-1] + arr2[n/2])/2;
  }

  return med;
}

// Function to find the average of an array of size n
double avg(double *arr, int n){

  int i=0;
  double sum=0, av=0;
  for (i = 0; i<n; i++){
    sum = sum+arr[i];
  }

  av = sum/(double)n;
  return av;
}


//Function to perform both a median and mean filter on a data set
void DataFilter(double meas, double raw_data[], double med_data[], double *filt){

  int i=0;

  // need to pass in a vector of  raw data points the same size as NUM_MED larger

  // Error check indicates if the length of the median vector is greate than average.
  // if this is allowed, the average function will go outside of the array
  if (NUM_AVG > NUM_MED){
    printf("\nERROR: NUM_AVG > NUM_MED = bad\n");
    return;
  }

  // enter new measurement as the most recent entry in the raw_data vector
  for (i=NUM_MED-1; i>0; i--){
    raw_data[i] = raw_data[i-1];
    //do the same thing with the median
    med_data[i] = med_data[i-1];
  }
  raw_data[0] = meas;


  // run raw data through median filter and create a single data point
  // assign that to the most recent entry in med_data
  med_data[0] = median(raw_data, NUM_MED);

  //average the last NUM_AVG values in the med_data array to get the current measurement, pass that back to the main to do something itneresting with
  double result = avg(med_data,NUM_AVG); 
  *filt = result;
}


