# Add your targets (i.e. the name of your output program) here
BINARIES = pixy_driver pixy_example

CC = gcc
CFLAGS = -g -Wall -lgsl -lgslcblas -lm -std=gnu99 `pkg-config --cflags lcm` `pkg-config --cflags gsl`
LDFLAGS = `pkg-config --libs lcm` `pkg-config --libs gsl` -lgsl -lgslcblas -lm -lpthread
BINARIES := $(addprefix bin/,$(BINARIES))


.PHONY: all clean lcmtypes bbblib

all: lcmtypes bbblib imu_driver FlightCode $(BINARIES)

lcmtypes:
	@$(MAKE) -C lcmtypes

bbblib:
	@$(MAKE) -C bbblib

clean:
	@$(MAKE) -C lcmtypes clean
	@$(MAKE) -C bbblib clean
	rm -f *~ *.o bin/*

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

lcmtypes/%.o: lcmtypes/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

pixy_operations: pixy_operations.o 
	$(CC) $^ -o pixy_operations -lm

bin/pixy_driver: pixy_driver.o util.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example: pixy_example.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o lcmtypes/imu_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

# Add build commands for your targets here

bin/pixy_example_threaded: pixy_example_threaded.o bbblib/gnc.o 
#lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o lcmtypes/imu_t.o pixy_operations.o
#	$(CC) -o $@ $^ $(LDFLAGS) -lm

FlightCode: FlightCode.o bbblib/bbb_pwm.o bbblib/gnc.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o lcmtypes/imu_t.o pixy_operations.o bbblib/bbb_init.o bbblib/kinematics.o command.o
	$(CC) $^ -o bin/FlightCode $(LDFLAGS) -lm

imu_driver: imu_driver.o  util.o bbblib/bbb_i2c.o bbblib/bbb_init.o lcmtypes/imu_t.o
	$(CC) $^ -o imu_driver $(LDFLAGS)
